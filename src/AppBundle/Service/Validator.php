<?php

namespace AppBundle\Service;

use AppBundle\Infrastructure\Validation\ValidatorInterface;

class Validator
{
    private CONST DATA_NOT_FOUND = 'Data is incorrect or lost';

    private $fields;

    public function add(string $name, ValidatorInterface $validator, array $options = []): self
    {
        $validator->setOptions($options);

        $this->fields[$name] = $validator;

        return $this;
    }

    public function validate($values)
    {
        /** @var ValidatorInterface $validator */
        foreach ($this->fields as $key => $validator) {
            $value = null;

            if (array_key_exists($key, $values)) {
                $value = $values[$key];
            }

            $values[$key] = $validator->validate($value, $key);
        }

        return $values;
    }
}