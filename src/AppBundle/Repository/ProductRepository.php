<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;

class ProductRepository
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function insertProductData(array $productData)
    {
        $qb = $this->em->getConnection()->createQueryBuilder();

        $sql = $qb
            ->insert('tblProductData')
            ->values ($productData);

        return $this->em->getConnection()->prepare($sql)->execute();
    }
}
