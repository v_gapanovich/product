<?php

namespace AppBundle\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Builder\CSVProductBuilder;

class ImportProductsFromCSV extends ContainerAwareCommand
{
    protected CONST SUCCESS = 'success: %s';
    protected CONST FAIL = 'fail: %s';
    protected CONST TOTAL = 'total: %s';

    private $em;
    private $builder;

    public function __construct(
        EntityManagerInterface $em,
        CSVProductBuilder $builder
    ) {
        $this->em = $em;
        $this->builder = $builder;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('products:import')
            ->addOption('test')
            ->addArgument('file', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $isTest = $input->getOption('test');
        $fileName = $input->getArgument('file');

        list($success, $fail) = $this->builder->buildProducts($fileName, $isTest);

        $response =  [
            sprintf(self::SUCCESS, $success),
            sprintf(self::FAIL, $fail),
            sprintf(self::TOTAL, $success + $fail)
        ];

        $output->writeln($response);
    }
}