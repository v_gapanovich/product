<?php

namespace AppBundle\Infrastructure;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use AppBundle\Service\Validator;
use AppBundle\Repository\ProductRepository;
use AppBundle\Infrastructure\Validation\Types\FloatType;
use AppBundle\Infrastructure\Validation\Types\StringType;
use AppBundle\Infrastructure\Validation\Types\IntegerType;

abstract class AbstractProductsBuilder
{
    protected CONST ERROR_FILE_NOT_FOUND = 'File not found';
    protected CONST IS_DISCONTINUED = 'yes';

    protected CONST LINE = 'line: %s. %s';
    protected CONST QUOTES = '\'%s\'';

    protected $product;
    protected $currentEntry;
    protected $totalSuccess = 0;
    protected $totalFailure = 0;
    protected $isTest = false;

    protected $em;
    protected $logger;
    protected $validator;
    protected $productRepository;

    public function __construct(
        ProductRepository $productRepository,
        Validator $validator,
        LoggerInterface $logger,
        EntityManagerInterface $em
    ) {
        $this->productRepository = $productRepository;
        $this->validator = $validator;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function buildProducts(string $fileName, bool $isTest = false)
    {
        $data = $this->setData($fileName, $isTest);
        $this->buildValidator();

        for ($i = 0; $i < count($data); $i++) {
            $this->currentEntry = $data[$i];
            $this->validateData();
        }

        return [$this->totalSuccess, $this->totalFailure];
    }

    protected function validateData()
    {
        try {
            $this->currentEntry = $this->validator->validate($this->currentEntry);
            $this->isDiscontinued()->saveProduct();

            $this->totalSuccess++;
        } catch (\Throwable $exception) {
            $this->totalFailure++;
            $this->logger->critical(
                sprintf(
                    self::LINE,
                    $this->totalSuccess + $this->totalFailure + 1,
                    $exception->getMessage()
                )
            );
        }
    }

    protected function isDiscontinued()
    {
        if (
            isset($this->currentEntry['Discontinued'])
            && $this->currentEntry['Discontinued'] === self::IS_DISCONTINUED
        ) {
            $this->currentEntry['Discontinued'] = (new \DateTime())->format('Y-m-d H:i:s');
        } else {
            $this->currentEntry['Discontinued'] = null;
        }

        return $this;
    }

    protected function saveProduct()
    {
        if (!$this->isTest && (
                $this->currentEntry['Cost in GBP'] >= 5 || $this->currentEntry['Stock'] >= 10
                && $this->currentEntry['Cost in GBP'] <= 1000
            )
        ) {
            $productData = [
                'strProductCode' => $this->addedQuotes($this->currentEntry['Product Code']),
                'strProductName' => $this->addedQuotes($this->currentEntry['Product Name']),
                'strProductDesc' => $this->addedQuotes($this->currentEntry['Product Description']),
                'intStock' => $this->addedQuotes($this->currentEntry['Stock']),
                'fltCost' => $this->addedQuotes($this->currentEntry['Cost in GBP']),
                'dtmAdded' => $this->addedQuotes((new \DateTime())->format('Y-m-d H:i:s'))
            ];

            if (!empty($this->currentEntry['Discontinued'])) {
                $productData['dtmDiscontinued'] = $this->addedQuotes($this->currentEntry['Discontinued']);
            }

            $this->productRepository->insertProductData($productData);
        }

        return $this;
    }

    private function addedQuotes($variable)
    {
        return sprintf(self::QUOTES, $variable);
    }

    public function buildValidator()
    {
        $this->validator
            ->add('Product Code', new StringType(), [
                'require' => true,
                'max' => 10,
                'min' => 1
            ])
            ->add('Product Name', new StringType(), [
                'require' => true,
                'max' => 50,
                'min' => 1
            ])
            ->add('Product Description', new StringType(), [
                'require' => true,
                'max' => 255,
                'min' => 1
            ])
            ->add('Stock', new IntegerType(), [
                'require' => true
            ])
            ->add('Cost in GBP', new FloatType())
            ->add('Discontinued', new StringType(), [
                'require' => false,
                'max' => 3
            ]);
    }

    public abstract function setData(string $fileName, bool $isTest): array;
}