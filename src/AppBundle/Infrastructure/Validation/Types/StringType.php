<?php

namespace AppBundle\Infrastructure\Validation\Types;

use Symfony\Component\Asset\Exception\InvalidArgumentException;
use AppBundle\Infrastructure\Validation\Exceptions\StringValidationFailed;
use AppBundle\Infrastructure\Validation\ValidatorInterface;

class StringType implements ValidatorInterface
{
    public const INCORRECT_OPTION_VALUE_ERROR = 'Incorrect option value';
    public const NOT_STRING_ERROR_TYPE = 'not string';
    public const REQUIRE_STRING_ERROR_TYPE = 'require string';
    public const MAX_ERROR_TYPE = 'max';
    public const MIN_ERROR_TYPE = 'min';

    private $options;
    private $value;
    private $name;

    public function validate($value, string $name): ?string
    {
        $this->value = is_string($value) ? trim($value) : $value;
        $this->name = $name;

        $this->required();
        $this->type();
        $this->max();
        $this->min();

        return addslashes($this->value);
    }

    public function required()
    {
        $required = true;

        if (array_key_exists('require', $this->options)) {
            if (is_bool($this->options['require'])) {
                $required = $this->options['require'];
            } else {
                throw new InvalidArgumentException(self::INCORRECT_OPTION_VALUE_ERROR);
            }
        }

        if ($required && empty($this->value)) {
            $this->getInvalidMessage(self::REQUIRE_STRING_ERROR_TYPE);
        }
    }

    public function type()
    {
        if (!empty($this->value) && !is_string($this->value)) {
            $this->getInvalidMessage(self::NOT_STRING_ERROR_TYPE);
        }
    }

    public function max()
    {
        if (
            array_key_exists('max', $this->options)
            && $this->isPositiveInt($this->options['max'])
        ) {
            if (mb_strlen($this->value) > $this->options['max']) {
                $this->getInvalidMessage(self::MAX_ERROR_TYPE);
            }
        }
    }

    public function min()
    {
        if (
            array_key_exists('min', $this->options)
            && $this->isPositiveInt($this->options['min'])
        ) {
            if (mb_strlen($this->value) < $this->options['min']) {
                $this->getInvalidMessage(self::MIN_ERROR_TYPE);
            }
        }
    }

    public function getInvalidMessage(string $type)
    {
        if (
            array_key_exists('invalid_message', $this->options)
            && $this->isStrNotEmpty($this->options['invalid_message'])
        ) {
            throw new StringValidationFailed($this->options['invalid_message']);
        }

        throw new StringValidationFailed(sprintf(self::DEFAULT_INVALID_MESSAGE, $this->name, $type));
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    protected function isStrNotEmpty($variable): bool
    {
        return is_string($variable) && !empty($variable);
    }

    protected function isPositiveInt($variable): bool
    {
        return is_int($variable) && $variable > 0;
    }
}