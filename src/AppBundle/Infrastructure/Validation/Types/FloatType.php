<?php

namespace AppBundle\Infrastructure\Validation\Types;

use Symfony\Component\Asset\Exception\InvalidArgumentException;
use AppBundle\Infrastructure\Validation\Exceptions\FloatValidationFailed;
use AppBundle\Infrastructure\Validation\ValidatorInterface;

class FloatType implements ValidatorInterface
{
    public const INCORRECT_OPTION_VALUE_ERROR = 'Incorrect option value';
    public const NOT_FLOAT_ERROR_TYPE = 'not float';
    public const REQUIRE_FLOAT_ERROR_TYPE = 'require float';
    public const MAX_ERROR_TYPE = 'max';
    public const MIN_ERROR_TYPE = 'min';

    private $options;
    private $value;
    private $name;

    public function validate($value, string $name)
    {
        $this->value = $value;
        $this->name = $name;

        $this->required();
        $this->type();
        $this->max();
        $this->min();

        return (float) $this->value;
    }

    public function required()
    {
        $required = true;

        if (array_key_exists('require', $this->options)) {
            if (is_bool($this->options['require'])) {
                $required = $this->options['require'];
            } else {
                throw new InvalidArgumentException(self::INCORRECT_OPTION_VALUE_ERROR);
            }
        }

        if ($required && empty($this->value) && !$this->isFloat($this->value)) {
            $this->getInvalidMessage(self::REQUIRE_FLOAT_ERROR_TYPE);
        }
    }

    public function type()
    {
        if (!empty($this->value) && !$this->isFloat($this->value)) {
            $this->getInvalidMessage(self::NOT_FLOAT_ERROR_TYPE);
        }
    }

    public function max()
    {
        if (
            array_key_exists('max', $this->options)
            && $this->isPositiveInt($this->options['max'])
        ) {
            if ($this->value > $this->options['max']) {
                $this->getInvalidMessage(self::MAX_ERROR_TYPE);
            }
        }
    }

    public function min()
    {
        if (
            array_key_exists('min', $this->options)
            && $this->isPositiveInt($this->options['min'])
        ) {
            if ($this->value < $this->options['min']) {
                $this->getInvalidMessage(self::MIN_ERROR_TYPE);
            }
        }
    }

    public function getInvalidMessage(string $type)
    {
        if (
            array_key_exists('invalid_message', $this->options)
            && $this->isStrNotEmpty($this->options['invalid_message'])
        ) {
            throw new FloatValidationFailed($this->options['invalid_message']);
        }

        throw new FloatValidationFailed(sprintf(self::DEFAULT_INVALID_MESSAGE, $this->name, $type));
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    private function isStrNotEmpty($variable): bool
    {
        return is_string($variable) && !empty($variable);
    }

    private function isPositiveInt($variable): bool
    {
        return is_int($variable) && $variable > 0;
    }

    private function isFloat($variable): bool
    {
        return is_numeric($variable) || is_float($variable);
    }
}