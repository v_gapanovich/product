<?php

namespace AppBundle\Infrastructure\Validation;

interface ValidatorInterface
{
    public CONST DEFAULT_INVALID_MESSAGE = 'Error variable: %s violated: %s';

    public function validate($value, string $name);

    public function setOptions(array $options);

    public function getInvalidMessage(string $type);

    public function type();
}