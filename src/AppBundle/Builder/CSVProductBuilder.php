<?php

namespace AppBundle\Builder;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Serializer;
use AppBundle\Infrastructure\AbstractProductsBuilder;

class CSVProductBuilder extends AbstractProductsBuilder
{
    public function setData(string $fileName, bool $isTest = false): array
    {
        $data = false;
        $this->isTest = $isTest;

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        if (file_exists($fileName)) {
            $data = $serializer->decode(
                file_get_contents($fileName),
                'csv'
            );
        }

        if ($data === false) {
            throw new \Exception(AbstractProductsBuilder::ERROR_FILE_NOT_FOUND);
        }

        return $data;
    }
}