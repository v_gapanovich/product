<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180713190041 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE tblProductData (
                intProductDataId int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                strProductName VARCHAR(50) NOT NULL,
                strProductDesc VARCHAR(255) NOT NULL,
                strProductCode VARCHAR(10) NOT NULL,
                dtmAdded DATETIME DEFAULT NULL,
                dtmDiscontinued DATETIME DEFAULT NULL,
                stmTimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (intProductDataId),
                UNIQUE KEY (strProductCode)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT=\'Stores product data\';
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tblProductData');
    }
}
