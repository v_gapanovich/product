<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180713190553 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            ALTER TABLE tblProductData
                ADD intStock int(11) NOT NULL;
        ');
        $this->addSql('
            ALTER TABLE tblProductData
                ADD fltCost FLOAT(11,2) NOT NULL;
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            ALTER TABLE tblProductData
                DROP COLUMN intCost;
        ');
        $this->addSql('
            ALTER TABLE tblProductData
                DROP COLUMN floatStock;
        ');
    }
}
