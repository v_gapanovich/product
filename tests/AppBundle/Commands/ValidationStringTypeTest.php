<?php

namespace Tests\AppBundle\Commands;

use PHPUnit\Framework\TestCase;
use AppBundle\Service\Validator;
use AppBundle\Infrastructure\Validation\Types\StringType;
use AppBundle\Infrastructure\Validation\Exceptions\StringValidationFailed;

class ValidationStringTypeTest extends TestCase
{
    public function testIsInteger()
    {
        $invalidData = ['name' => ['string']];
        $validator = (new Validator())
            ->add('name', new StringType(), [
                'require' => true
            ]);

        try {
            $validator->validate($invalidData);
        } catch (StringValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), StringType::NOT_STRING_ERROR_TYPE) !== false
            );
        }
    }

    public function testIntegerIsRequired()
    {
        $invalidData = ['name' => null];
        $validator = (new Validator())
            ->add('name', new StringType(), [
                'require' => true
            ]);

        try {
            $validator->validate($invalidData);
        } catch (StringValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), StringType::REQUIRE_STRING_ERROR_TYPE) !== false
            );
        }
    }

    public function testIntegerMaxValue()
    {
        $invalidData = ['name' => 'string'];
        $validator = (new Validator())
            ->add('name', new StringType(), [
                'max' => 5
            ]);

        try {
            $validator->validate($invalidData);
        } catch (StringValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), StringType::MAX_ERROR_TYPE) !== false
            );
        }
    }

    public function testIntegerMinValue()
    {
        $invalidData = ['name' => 'str'];
        $validator = (new Validator())
            ->add('name', new StringType(), [
                'min' => 5
            ]);

        try {
            $validator->validate($invalidData);
        } catch (StringValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), StringType::MIN_ERROR_TYPE) !== false
            );
        }
    }
}