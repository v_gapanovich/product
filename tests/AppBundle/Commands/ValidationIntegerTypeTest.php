<?php

namespace Tests\AppBundle\Commands;

use PHPUnit\Framework\TestCase;
use AppBundle\Service\Validator;
use AppBundle\Infrastructure\Validation\Types\IntegerType;
use AppBundle\Infrastructure\Validation\Exceptions\IntegerValidationFailed;

class ValidationIntegerTypeTest extends TestCase
{
    public function testIsInteger()
    {
        $invalidData = ['code' => 1.1];
        $validator = (new Validator())
            ->add('code', new IntegerType(), [
                'require' => true
            ]);

        try {
            $validator->validate($invalidData);
        } catch (IntegerValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), IntegerType::NOT_INT_ERROR_TYPE) !== false
            );
        }
    }

    public function testIntegerIsRequired()
    {
        $invalidData = ['code' => null];
        $validator = (new Validator())
            ->add('code', new IntegerType(), [
                'require' => true
            ]);

        try {
            $validator->validate($invalidData);
        } catch (IntegerValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), IntegerType::REQUIRE_INT_ERROR_TYPE) !== false
            );
        }
    }

    public function testIntegerMaxValue()
    {
        $invalidData = ['code' => 100];
        $validator = (new Validator())
            ->add('code', new IntegerType(), [
                'max' => 10
            ]);

        try {
            $validator->validate($invalidData);
        } catch (IntegerValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), IntegerType::MAX_ERROR_TYPE) !== false
            );
        }
    }

    public function testIntegerMinValue()
    {
        $invalidData = ['code' => 4];
        $validator = (new Validator())
            ->add('code', new IntegerType(), [
                'min' => 5
            ]);

        try {
            $validator->validate($invalidData);
        } catch (IntegerValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), IntegerType::MIN_ERROR_TYPE) !== false
            );
        }
    }
}