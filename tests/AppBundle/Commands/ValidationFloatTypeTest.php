<?php

namespace Tests\AppBundle\Commands;

use PHPUnit\Framework\TestCase;
use AppBundle\Service\Validator;
use AppBundle\Infrastructure\Validation\Types\FloatType;
use AppBundle\Infrastructure\Validation\Exceptions\FloatValidationFailed;

class ValidationFloatTypeTest extends TestCase
{
    public function testFloatIsRequired()
    {
        $invalidData = ['cost' => null];
        $validator = (new Validator())
            ->add('cost', new FloatType(), [
                'require' => true
            ]);

        try {
            $validator->validate($invalidData);
        } catch (FloatValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), 'require') !== false
            );
        }
    }

    public function testFloatMaxValue()
    {
        $invalidData = ['cost' => 100];
        $validator = (new Validator())
            ->add('cost', new FloatType(), [
                'max' => 10
            ]);

        try {
            $validator->validate($invalidData);
        } catch (FloatValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), 'max') !== false
            );
        }
    }

    public function testFloatMinValue()
    {
        $invalidData = ['cost' => 4.99];
        $validator = (new Validator())
            ->add('cost', new FloatType(), [
                'min' => 5
            ]);

        try {
            $validator->validate($invalidData);
        } catch (FloatValidationFailed $exception) {
            $this->assertEquals(
                true,
                strpos($exception->getMessage(), 'min') !== false
            );
        }
    }
}