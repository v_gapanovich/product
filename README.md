# Command for import product

`bin/console product:import app/Resources/csv/stock.csv --test`

* **'app/Resources/csv/stock.csv'** - csv file with data

* **'--test'** - option for enabling test mode